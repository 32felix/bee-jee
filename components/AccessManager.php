<?php
namespace app\components;

use app\controllers\SiteController;
use app\controllers\TaskController;

class AccessManager
{
    private $routes;
    private $regex;

    public function getRegex($id=null)
    {
        $regex = null;
        $c = false;
        if(!$id && !empty($_SESSION['userId']))
        {
            if ($this->regex===null)
            {
                $c = true;
                $regex = !empty($_SESSION['regex']) ? $_SESSION['regex'] : null;
                $id = $_SESSION['userId'];
            }
        }

        if (!is_array($regex) && $id)
        {
            $db = Db::getConnection();
            $result = $db->query("SELECT DISTINCT(access) as field FROM(
                    SELECT * FROM AccessUser AU WHERE AU.userId=" . $id . "
                    UNION
                    SELECT * FROM AccessGroup AG WHERE AG.groupId IN (SELECT UGU.groupId FROM UserGroupUser UGU WHERE UGU.userId= " . $id . ")
                  )  T");

            $regex = $result->fetch_all(MYSQLI_ASSOC);

            foreach ($regex as $key=>$items)
            {
                $regex[$key] = $items['field'];
            }

            if ($c)
                $_SESSION['regex'] = $regex;
        }

        if (is_array($regex) && $id)
        {
            $this->regex = "^((".implode(')|(', $regex)."))$";

            return $this->regex;

        }
        else
            return "^guest$";
    }

    public static function getAccess ($access)
    {
        $regex = (new self)->getRegex();

        if (preg_match('~'.$regex.'~', $access))
            return true;

        return false;
    }
}