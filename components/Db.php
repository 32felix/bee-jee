<?php
namespace app\components;

use mysqli;

class Db
{
    public static function getConnection ()
    {
        $paramPath = ROOT . '/config/db.php';
        $params = include($paramPath);

        $db = new mysqli($params['host'], $params['username'], $params['password'], $params['dbname']);

        return $db;
    }
}