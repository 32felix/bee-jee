<?php
namespace app\components;

use app\controllers\SiteController;
use app\controllers\AdminController;
use app\controllers\TaskController;

class Router
{
    private $routes;
    private $regex;

    public function __construct()
    {
        $routerPath = ROOT . '/config/routes.php';
        $this->routes = include($routerPath);
    }

    public function run()
    {
        $uri =  $this->getURI();

        foreach ($this->routes as $urlPattern=>$path) {
            if (preg_match("~$urlPattern~", $uri))
            {
                $internalRoute = preg_replace("~$urlPattern~", $path, $uri);
                $segments = explode('/', $internalRoute);

                $controllerName = ucfirst(array_shift($segments)) . 'Controller';
                $actionName = 'action' . ucfirst(array_shift($segments));

                $controllerFile = ROOT . '/controllers/' . $controllerName . '.php';

                $parameters = $segments;

                if (file_exists($controllerFile))
                    include_once($controllerFile);

                $controllers = $this->getControllers();

                if (!empty($controllers[$controllerName]))
                {
                    $controllerObject = new $controllers[$controllerName];
                    $result = call_user_func_array([$controllerObject, $actionName], $parameters);
                }

                if (isset($result))
                    break;
            }
        }
    }

    /**
     * Return request string
     * @return array
     */
    private function getControllers()
    {
       return [
           'SiteController' => SiteController::class,
           'TaskController' => TaskController::class,
       ];
    }

    /**
     * Return request string
     * @return string
     */
    private function getURI ()
    {
        if (!empty($_SERVER['REQUEST_URI']))
            return trim($_SERVER['REQUEST_URI'], '/');

        return false;
    }

    public static function redirect ($url, $permanent = false)
    {
        header('Location: ' . $url, true, $permanent ? 301 : 302);
        exit();
    }
}