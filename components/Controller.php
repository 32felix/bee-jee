<?php
namespace app\components;

class Controller
{
    public function __construct()
    {
        session_start();

        require_once(ROOT . '/components/AccessManager.php');
        require_once(ROOT . '/models/Task.php');
        require_once(ROOT . '/models/search/TaskSearch.php');
        require_once(ROOT . '/models/form/LoginForm.php');
    }
}