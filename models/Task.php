<?php
namespace app\models;

use app\components\Db;

class Task
{
    public $id;
    public $name;
    public $email;
    public $text;
    public $status = 'new';

    public function create()
    {
        if (empty($this->name) || empty($this->email) || empty($this->text))
            return false;

        $db = Db::getConnection();

        $result = $db->query("INSERT INTO `Task`(`name`,`email`,`text`) VALUES 
            (".json_encode($this->name, JSON_UNESCAPED_UNICODE).",".json_encode($this->email, JSON_UNESCAPED_UNICODE).","
            .json_encode($this->text, JSON_UNESCAPED_UNICODE).")");

        if ($result)
            return true;

        return false;
    }

    public function edit()
    {
        if (empty($this->id) || empty($this->text) || empty($this->status) || !in_array($this->status, array_keys(self::getStatuses())))
            return false;

        $db = Db::getConnection();

        if ($this->id)
            $result = $db->query("SELECT * FROM Task WHERE id='".$this->id."'");

        if (!empty($result) && $task = $result->fetch_array(MYSQLI_ASSOC))
        {
            $result = $db->query("UPDATE Task 
             SET text='".json_encode($this->text, JSON_UNESCAPED_UNICODE)."', status='".$this->status."'
             WHERE id='".$this->id."'");

            if ($result)
                return true;
        }
        return false;
    }

    public function getTask($id=null)
    {
        $db = Db::getConnection();

        if (!$id)
            $id = $this->id ;

        if (!$id)
            return false;

        $result = $db->query("SELECT * FROM Task WHERE id='".$id."'");
        return $result->fetch_array(MYSQLI_ASSOC);
    }

    public static function getStatuses()
    {
        return [
            'new' => 'Новая',
            'finished' => 'Завершенная'
        ];
    }
}