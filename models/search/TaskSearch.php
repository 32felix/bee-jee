<?php
namespace app\models\search;

use app\components\Db;

class TaskSearch
{
    public $countRow = 3;

    public function getTasks($page, $sort='timeCreate')
    {
        $sort_dir = 'ASC';

        if (substr($sort, 0, 1) == '-')
        {
            $sort_dir = 'DESC';
        }

        if (trim($sort) == '' || !in_array($sort, self::getSortFields()))
            $sort = 'timeCreate';

        if (trim($page) == '')
            $page = 1;



        $db = Db::getConnection();

        $result = $db->query("SELECT * FROM Task ORDER BY `" . $sort . "` " . $sort_dir . "
                                     LIMIT " . ($page - 1) * $this->countRow . ", " . $this->countRow,
             MYSQLI_USE_RESULT);

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getCountPages()
    {
        $db = Db::getConnection();

        $result = $db->query("SELECT count(1) as count FROM Task",
            MYSQLI_USE_RESULT);

        $tasks = $result->fetch_array(MYSQLI_ASSOC);

        return $tasks['count'] / $this->countRow;
    }

    private function getSortFields()
    {
        return [
            'name',
            'email',
            'status',
            'timeCreate',
        ];
    }
}