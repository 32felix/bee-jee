<?php
namespace app\models\form;

use app\components\AccessManager;
use app\components\Db;
use app\components\Router;

class LoginForm
{
    public $login;
    public $password;

    private $_user = false;

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     */
    public function validatePassword()
    {
        if (!$this->getUser() || md5($this->password)!=$this->_user['pass'])
        {
            return false;
        }

        return true;
    }


    /**
     * Logs in a user using the provided username and password.
     * @return boolean whether the user is logged in successfully
     */
    public function login($post)
    {
        if ($this->validatePassword())
        {
            if (!empty($_SESSION['userId']))
                $_SESSION = [];

            $_SESSION['userId'] = $this->_user['id'];
            $_SESSION['userName'] = $this->_user['name'];
            $_SESSION['userCreate'] = $this->_user['timeCreate'];

            (new AccessManager)->getRegex();

            return true;
        }

        return false;
    }


    public function getUser()
    {
        if ($this->_user === false)
        {
            $db = Db::getConnection();
            $result = $db->query("SELECT * FROM `User` WHERE login=" . json_encode($this->login, JSON_UNESCAPED_UNICODE));
            $this->_user = $result->fetch_array(MYSQLI_ASSOC);
        }

        return $this->_user;
    }

}
