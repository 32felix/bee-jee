<?php

use app\components\AccessManager;
use app\models\Task;

$statuses = Task::getStatuses();
?>
    <table class="table table-bordered table-hover">
        <tr>
            <th>
                <a href="/<?= $page ?>/<?= $sort != 'name' ? 'name' : '-name' ?>">
                    Имя пользователя
                    <?= $sort == 'name' ? '<i class="glyphicon glyphicon-sort-by-alphabet"></i>' : '' ?>
                    <?= $sort == '-name' ? '<i class="glyphicon glyphicon-sort-by-alphabet-alt"></i>' : '' ?>
                </a>
            </th>

            <th>
                <a href="/<?= $page ?>/<?= $sort != 'email' ? 'email' : '-email' ?>">
                    E-mail
                    <?= $sort == 'email' ? '<i class="glyphicon glyphicon-sort-by-alphabet"></i>' : '' ?>
                    <?= $sort == '-email' ? '<i class="glyphicon glyphicon-sort-by-alphabet-alt"></i>' : '' ?>
                </a>
            </th>

            <th>Текст задачи</th>

            <th>
                <a href="/<?= $page ?>/<?= $sort != 'status' ? 'status' : '-status' ?>">
                    Статус
                    <?= $sort == 'status' ? '<i class="glyphicon glyphicon-sort-by-alphabet"></i>' : '' ?>
                    <?= $sort == '-status' ? '<i class="glyphicon glyphicon-sort-by-alphabet-alt"></i>' : '' ?>
                </a>
            </th>

            <?= AccessManager::getAccess('admin') ? '<th></th>' : '' ?>
        </tr>

        <? foreach ($list as $item): ?>
            <tr>
                <td><?= htmlentities($item['name']) ?></td>
                <td><?= htmlentities($item['email']) ?></td>
                <td><?= htmlentities($item['text']) ?></td>
                <td><?= $statuses[$item['status']] ?? $item['status'] ?></td>
                <?= AccessManager::getAccess('admin') ?
                    '<td><a href="/task/edit/' . $item['id'] . '" class="glyphicon glyphicon-pencil"></a></td>' : '' ?>
            </tr>
        <? endforeach; ?>
    </table>

<? if ($countPages > 1): ?>
    <ul class="pagination">
        <li><a href="/<?= trim($sort) != '' ? '1/' . $sort : '' ?>">«</a></li>
        <? for ($i = 0; $i <= $countPages; $i++): ?>
            <li><a href="/<?= ($i + 1) . (trim($sort) != '' ? '/' . $sort : '') ?>"><?= $i + 1 ?></a></li>
        <? endfor; ?>
        <li><a href="/<?= ceil($countPages) . (trim($sort) != '' ? '/' . $sort : '') ?>">»</a></li>
    </ul>
<? endif; ?>