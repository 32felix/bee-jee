<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
<div class="site-login">
    <h1>Создать задачу</h1>

    <form method="post" enctype="multipart/form-data">
        <div class="form-group">
            <label for="name">Имя пользователя</label>
            <input type="text" id="name" name="name" class="form-control" value="<?= isset($_POST['name']) ? $_POST['name'] : '' ?>" required />
        </div>
        <div class="form-group">
            <label for="email">E-mail</label>
            <input type="email" id="email" name="email" class="form-control" value="<?= isset($_POST['email']) ? $_POST['email'] : '' ?>" required />
        </div>
        <div class="form-group">
            <label for="text">Текст задачи</label>
            <textarea id="text" name="text" class="form-control" required><?= isset($_POST['text']) ? $_POST['text'] : '' ?></textarea>
        </div>

        <div class="form-group">
            <div class="col-lg-offset-1 col-lg-11">
                <input type="submit" class='btn btn-primary' name='create-button' value="Создать" />
            </div>
        </div>
    </form>
</div>