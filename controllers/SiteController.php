<?php
namespace app\controllers;

use app\components\Controller;
use app\components\Router;
use app\models\form\LoginForm;
use app\models\search\TaskSearch;
use app\models\Task;

class SiteController extends Controller
{

    public function actionIndex($page=1, $sort=null)
    {
        $model = new TaskSearch();
        $list = $model->getTasks($page, $sort);
        $countPages = $model->getCountPages();
        $view = ROOT . '/views/site/index.php';
        require_once(ROOT . '/views/layouts/main.php');
        return true;
    }

    public function actionLogin()
    {
        $model = new LoginForm();

        if (isset($_POST['login']) && isset($_POST['password']))
        {
            $model->login = $_POST['login'];
            $model->password = $_POST['password'];

            if ($model->login())
                return Router::redirect('/');

        }

        $view = ROOT . '/views/site/login.php';
        require_once(ROOT . '/views/layouts/main.php');

        return true;
    }

    public function actionLogout()
    {
        if ($_SESSION['userId'])
            $_SESSION = [];

        return Router::redirect('/');
    }
}
