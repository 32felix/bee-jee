<?php

namespace app\controllers;

use app\components\AccessManager;
use app\components\Controller;
use app\components\Router;
use app\models\Task;

class TaskController extends Controller
{

    public function actionCreate()
    {
        $model = new Task();

        $model->name = $_POST['name'] ?? null;
        $model->email = $_POST['email'] ?? null;
        $model->text = $_POST['text'] ?? null;

        if ($model->create())
        {
            return Router::redirect('/');
        }

        $view = ROOT . '/views/task/create.php';
        require_once(ROOT . '/views/layouts/main.php');
        return true;
    }

    public function actionEdit($id = null)
    {
        if (!AccessManager::getAccess('admin'))
        {
            return Router::redirect('/');
        }

        $model = new Task();
        $model->id = $id;
        $model->text = $_POST['text'] ?? null;
        $model->status = $_POST['status'] ?? null;

        if ($model->edit())
        {
            return Router::redirect('/');
        }

        $task = $model->getTask($id);

        if (!$task)
        {
            return Router::redirect('/task/create');
        }

        $view = ROOT . '/views/task/edit.php';
        require_once(ROOT . '/views/layouts/main.php');
        return true;
    }
}
